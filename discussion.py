# [Section] Lists
names = ["John", "Paul", "George", "Ringo"] # string list
programs = ['developer career', 'pi-shape', 'short courses'] # string list
durations = [260, 180, 20] # number list
truth_values = [True, False, True, True, False] # boolean list
sample_list = ["Apple", 3, False, 'Potato', 4, True]

# Getting list size
print(len(programs))

# accessing values
# first element
print(programs[0])
# last element
print(programs[-1])
# negative index
print(names[-3])

# print array list
print(durations)

# multiple values 
print(programs[0:2])

# [Section] Mini Activity
students = ["Mary", "Matthew", "Tom", "Anna", "Thomas"]
grades = [100, 85, 88, 90, 75]

count = 0
while count < len(students):
    print(f"The grade of {students[count]} is { grades[count] }")
    count += 1

# Updating Lists
print(programs[2])
programs[2] = "Short Courses"
print(f"New Value: {programs[2]}")

# [Section] List Manipulation
# append() - adds at the end of the list
programs.append('global')
print(programs)

# deleting of elements
durations.append(360)
print(durations)
del durations[-1]
print(durations)

# membershop checks
print(20 in durations)
print(500 in durations)
print("developer careers" in programs)

# sorting lists
print(names)
names.sort()
print(names)

# clear 
test_list = [1, 2, 3, 4, 5]
test_list.clear()
print(test_list)

# [Section] Disctionaries
person1 = {
	"name": "Brandon",
	"age": 28,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Phyton", "SQL", "Django"]
}

print(len(person1))

# accessing values
print(person1["name"])

# [Section] Dictionary Methods
print(person1.keys())
print(person1.values())
print(person1.items())

# addding key-value pairs
person1["nationality"] = "Filipino"
person1.update({"fave_food": "Sinigang" })
print(person1)

# delete key-value pairs
person1.pop("fave_food")
del person1["nationality"]
print(person1)

person2 = {
	"name": "John"
}
print(person2)
person2.clear()
print(person2)

# Looping through the dictionary
for key in person1:
	print(f"The value of {key} is {person1[key]}")

person3 = {
	"name": "Monika",
	"age": 20,
	"occupation": "poet",
	"isEnrolled": True,
	"subjects": ["Phyton", "SQL", "Djangco"]
}

classroom = {
	"student1": person1,
	"student2": person3
}
print(classroom)

# [Section] Mini Activity
car = {
    "brand" : "Toyota",
    "model" : "Vios",
    "year_of_make" : 2015,
    "color" : "Silver"
}
print(f"I own a {car['brand']} {car['model']}, and it was made in {car['year_of_make']}")

# [Section] Functions
def my_greeting():
	print("Hello User!")

my_greeting()

def greet_user(user):
	print(f"Hello {user}")

greet_user("Bob")
greet_user("Amy")

def addition(num1, num2):
        return num1 + num2

sum = addition(5, 10)
print(f"The sum is {sum}")

# [Section] Lambda functions
greeting = lambda person : f"hello {person}"

print(greeting("Elsie"))
print(greeting("Anthony"))

mult = lambda a, b : a * b 
result = mult(5, 6)
print(result)

# [Section] Mini Activity
def sqr(num):
    return num * num

# [Section] Classes
class Car():
	# Constructor
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# hard-coded values will set as default values
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# since we dont have a way to print the entire instance, repr() is used by python developers
	def _repr_():
		return "My car is {self.brand} {self.model} "

	# Methods
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print("filling up the fuel tank...")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")

	def drive(self, distance):
		print(f"The car has driven {distance} kilometer")
		print(f"The fuel level left: {self.fuel_level - distance}")
		self.fuel_level -= dustance

# Instantiate a new car
new_car = Car("Honda", "Jazz", "2020")

print(new_car.__repr__())

print(f"My car is a {new_car.brand}, {new_car.model}")
new_car.fill_fuel()
new_car.drive(50)

